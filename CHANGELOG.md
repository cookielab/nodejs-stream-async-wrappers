# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).
This change log adheres to standards from [Keep a CHANGELOG](http://keepachangelog.com).

## [Unreleased]
### Added
- Use `@cookielab.io/nodejs-backend-scripts` package

### Changed
- Updated dependencies
- Use `prepack` script instead of `prepare`

### Removed
- Remove support for Flow type
- Drop support for Node.js < 12.13.0

## [0.5.0] - 2020-06-17
### Changed
 - Require minimum Node.js version of 10.13
 - Updated dependencies
 - Made readable stream chunk type configurable by constructor generic

## [0.4.0] - 2019-08-29
### Changed
 - Made `writableStreamAsyncWriter.waitForDrain` private
 - Updated dependencies

## [0.3.0] - 2019-01-09
### Changed
 - Require minimum Node.js version of 8.10 
 - Rewritten into TypeScript
 - Updated dependencies

## [0.2.1] - 2018-07-21
### Added
 - Repository section into package.json

## [0.2.0] - 2018-06-21
### Changed
 - Updated dependencies

## [0.1.0] - 2018-04-24
Initial release
